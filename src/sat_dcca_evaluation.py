from itertools import combinations
import sys
import getopt
import subprocess
import itertools
import time
import datetime
import multiprocessing as mp
import shutil
__author__ = 'gonschorek'
__doc__ = "Helptext"
__runs__ = 10
__process_count__ = 4
#tool_parameter ([process_call + params], file_idx, file_param_type(0 =parameter, 1=read from stdin, 2=interactive param, 3=smv file with LTL[ U ] 4=interactive param with dcca_formula ), verify _result)
tool_parameters = [(["iimc"], 1, 0, 2, "aig"),(["nuxmv", "-int"], 1, 2, 1, "aig"),(["aigbmc", "100"], 1, 0, 0, "aig"),(["IC3"], 2, 1, 0, "aig"),(["nuxmv"],1 , 4, 4, "bdd.nusmv"),(["nuxmv", "-bmc", "-bmc_length", "76"],4 , 3, 3, "nusmv"),(["nusmv"],1 , 4, 4, "bdd.nusmv"),(["nusmv", "-bmc", "-bmc_length", "76"],4 , 3, 3, "nusmv")]# , (["k-ind_aig"], 1, 1, 0, "aig")


def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:l:mc:", ["help"], )
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options    for o, a in opts:
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
        if o in "-l":
            latches = a.split(',', )

        if o in "-f":
            file_path = a

    #parallel processing of the different analyzation techniques
    pool = mp.Pool(processes=__process_count__)

    for x in range(0, len(tool_parameters)):
        pool.apply_async(test_process, args=(tool_parameters[x], file_path, latches, x))#, callback=callback)

    pool.close()
    pool.join()

#    pool = mp.Pool(processes=__process_count__)
#    pool.map(test_process, [(tool_parameters[x], file_path,latches, x) for x in range(0,len(tool_parameters)-1)])
#   results.get(timeout=1)
#    for mc_param in tool_parameters:
#        test_process(mc_param, file_path,latches)


def test_process(mc_param, file_path, latches, pid):
    i = 0
    now = datetime.datetime.now()
    result_file = open(str(pid) + "/dccaresult_complete_" + file_path + "_" + now.strftime("%Y%m%d_%H%M") + ".csv","w")
    result_file.writelines("run;result;time;memory\n")
    (mc_call, file_idx, file_param_type, verify_result, file_ending) = mc_param

    for i in range(0, __runs__):
        now = datetime.datetime.now()
        result_file_single = open(str(pid) + "/dccaresult_single_" + file_path + "." + file_ending + "_" + str(i) + "_" + now.strftime("%Y%m%d_%H%M") + ".csv","w")
        sat_dcca(mc_call, file_idx, str(pid) + "/"+file_path + "." + file_ending, file_param_type, verify_result, latches, 1, result_file_single, result_file, i, pid)

        result_file_single.close()

    result_file.close()

    #return mc_param


def sat_dcca(mc_param, file_path_idx, file_path, file_param_type, verify_result, latches, it, result_file_single, result_file, run, pid):
    '''
    main entry point of the dcca algorithm
    :param mc_param:        param array for the model checking process containing path to mc and additional params like
                            file path etc.
    :param file_path_idx:   index pointing to the position of the file path in the mc parameter array. The file_path
                            will be inserted at that position into the mc_param array
    :param file_path:       path of the generated file to be verified
    :param latches:         index of the failure observer latches that should not be mentioned in the current
    :param it:              order of the failure set
    :return:                minimal cut set of the given model corresponding to the passed failureObserverLatches
    '''
    result_list = []
    eval_string = []
    run_stat = ""
    start_time = time.time()

    if file_param_type == 3 or file_param_type == 4:
        latches2 = []
        j = 1
        for i in latches:
            latches2.append(str(j))
            j += 1
        latches = latches2
    failures = derive_failurecombinations(latches, result_list, it)

    while len(failures) > 0:
        for failure in failures:
            failure = list(failure)
            if file_param_type == 3:
                file2test = generate_ltl_smv_file(file_path, failure, latches, pid)
            elif file_param_type == 4:
                file2test = generate_ctl_smv_file(file_path, failure, latches, pid)
            else:
                file2test = generate_file(file_path, failure, latches, pid)

            if verify(file2test, mc_param, file_path_idx, file_param_type, verify_result, eval_string) == "1":
                result_list.append(failure)

        it += 1
        failures = derive_failurecombinations(latches, result_list, it)

    endtime = time.time() - start_time
    #    print("--- %s seconds ---" % (time.time() - start_time))
    #    print("--- %s memory ---" % memory_usage_own())
    #    print eval_string
    result_file_single.writelines("call;result;time;memory\n")

    print "eval string"
    print eval_string

    result_file_single.write("".join(eval_string))
    run_stat += str(run)
    run_stat += ";"
    run_stat += "[ "
    #print result_list
    for result in result_list:
        run_stat += "["

        for fail in result:
            run_stat += str(fail)
            run_stat += ","

        run_stat += "]"
        run_stat += ","

    run_stat += " ];"

    #    eval_string += ",".join(result_list)

    run_stat += "{:.9f}".format(endtime)
    run_stat += ";"

    run_stat += "{:.9f}".format(memory_usage_resource())
    run_stat += '\n'
    result_file.writelines(run_stat)

    return result_list


def generate_file(basefile_path, failure, latches, pid):
    '''
    generates the aiger file with the desired failure as constraint
    :param basefile_path:   path of the initial model.aig file
    :param failure:         failure latches to be added as constrain, i.e., to be observed
    :param latches:         list, containing all available latches
    :return:
    '''
    fail_not_occur = "" #string for building the latch param (-l) for aig4mbsa tool
    for fa in latches:
        if not (fa in failure):
            fail_not_occur += fa + ","
    # cut last comma
    if len(fail_not_occur) > 0:
        fail_not_occur = fail_not_occur[:len(fail_not_occur)-1]
    # cut initial .aig extension and add [-failure]*.aag
    file_under_test = basefile_path[:len(basefile_path)-4] + name_from_tuple(failure) +".aag"
    subprocess.call([str(pid) + "/aig4mbsa", "-f", basefile_path, "-o", file_under_test, "-l", fail_not_occur], stdout=subprocess.PIPE, shell=False)

    return file_under_test


def generate_ltl_smv_file(basefile_path, failure, latches, pid):
    '''
    generates the aiger file with the desired failure as constraint
    :param basefile_path:   path of the initial model.aig file
    :param failure:         failure latches to be added as constrain, i.e., to be observed
    :param latches:         list, containing all available latches
    :return:
    '''
    fail_not_occur = "LTLSPEC G(!(" #string for building the latch param (-l) for aig4mbsa tool
    for fa in latches:
        if not (fa in failure):
            fail_not_occur += " f" + str(fa) + " |"
    # cut last comma
    if len(fail_not_occur) > 0:
        fail_not_occur = fail_not_occur[:len(fail_not_occur)-1]

    fail_not_occur += ") -> !hazard)"

    # cut initial .nusmv extension and add [-failure]*.smv
    last_dot_idx = basefile_path.rfind('.')
    file_under_test_path = basefile_path[:last_dot_idx] + name_from_tuple(failure) +".smv"
    shutil.copyfile(basefile_path, file_under_test_path)
    file_under_test = open(file_under_test_path, 'a')
    file_under_test.write(fail_not_occur)
    file_under_test.close()

    return file_under_test_path


def generate_ctl_smv_file(basefile_path, failure, latches, pid):
    '''
    generates the aiger file with the desired failure as constraint
    :param basefile_path:   path of the initial model.aig file
    :param failure:         failure latches to be added as constrain, i.e., to be observed
    :param latches:         list, containing all available latches
    :return:
    '''
    fail_not_occur = "SPEC E[!(" #string for building the latch param (-l) for aig4mbsa tool
    for fa in latches:
        if not (fa in failure):
            fail_not_occur += " f" + str(fa) + " |"
    # cut last comma
    if len(fail_not_occur) > 0:
        fail_not_occur = fail_not_occur[:len(fail_not_occur)-1]

    fail_not_occur += ") U hazard]"

    # cut initial .nusmv extension and add [-failure]*.smv
    last_dot_idx = basefile_path.rfind('.')
    file_under_test_path = basefile_path[:last_dot_idx] + name_from_tuple(failure) +".smv"
    shutil.copyfile(basefile_path, file_under_test_path)
    file_under_test = open(file_under_test_path, 'a')
    file_under_test.write(fail_not_occur)
    file_under_test.close()

    return file_under_test_path


def name_from_tuple(failures):
    '''
    build the name extension for the generated .aag file, e.g. "4-2" for failure set['4','2']
    :param  failures: list containing the failure set to build the name extension of
    :return string of the form "4-2" for failures=['4','2']
    '''
    f_string = ""
    for f in failures:
        f_string += "-" + f

    return f_string


def verify(file2test, mc_param, file_path_idx, file_param_type, verify_result, eval_string):
    '''
    executes the model checker and returns the verification result
    :param file2test:       input file to be tested
    :param mc_param:        parameter indeication the model checker and the parameters to be passed
    :param file_path_idx:   index of the file path position in the mc_param list
    :return:                the verification result
    '''

    run_stat = ""
    run_stat += ",".join(mc_param)
    run_stat += ";"
    #    print mc_param
    if file_param_type == 0 or file_param_type == 3 or file_param_type == 4:
        mc_param.insert(file_path_idx, file2test)

        start_time = time.time()

        p = subprocess.Popen(mc_param, stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=False)
        out, error = p.communicate()
        mc_param.remove(file2test)

    if file_param_type == 1:
      #  print mc_param

        f = open(file2test,'r')
        start_time = time.time()

        p = subprocess.Popen(mc_param, stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=False)
        out, error = p.communicate(f.read())

        f.close()

    if file_param_type == 2:
        com_str =  "read_aiger_model -i " + file2test + ";"
        com_str += "encode_variables" + ";"
        com_str += "build_boolean_model" + ";"
        com_str += "check_invar_ic3" + ";"
        com_str += "quit" + ";"
        start_time = time.time()

        p = subprocess.Popen(mc_param, stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=False)
        out, error = p.communicate(com_str)

    if verify_result == 0:
#        print out
        result = out[:1]
    if verify_result == 1:
#        print out
        pos = out.find("-- invariant")
        pos = out.find("  ", pos)
        result = out[pos+6:pos+11]
        if result.find("true") > -1:
            result = "0"
        if result.find("false") > -1:
            result = "1"
    if verify_result == 2:
#        print out
        pos = out.find("c ")
        if (pos == -1):
            result = out[len(out)-2:len(out)-1]
        else:
            result = out[pos-2:pos-1]
    if verify_result == 3:
#        print out
        result = ""
        pos = out.find("-- specification")
        if pos == -1:
            result = "0"
        pos = out.find("    ", pos)
        out = out[pos+7:pos+12]
        if out.find("false") > -1:
            result = "1"
    if verify_result == 4:
#        print out
        result = ""
        pos = out.find("-- specification")
        if pos == -1:
            result = "0"
        pos = out.find("   ", pos)
        out = out[pos+6:pos+11]
        if out.find("false") > -1:
            result = "0"
        if out.find("true") > -1:
            result = "1"

    #print result
    run_stat += result
    run_stat += ";"

    run_stat += "{:.9f}".format(time.time() - start_time)
    run_stat += ";"

    run_stat += "{:.9f}".format(memory_usage_resource())
    run_stat += '\n'
    eval_string.append(run_stat)
    return result #cuts '\n' at the end of the result string


def derive_failurecombinations(candidates, blacklist, l):
    '''
    builds next failure combinations that are not covered by already detected failures of the smaller order
    :param candidates:  possible failure modi to be combined to a new set
    :param blacklist:   already discovered minimal failure sets
    :param l:           size of the new failure sets
    :return:            list containing all failures still need to be verified
    '''
    return collect_combinations(candidates, blacklist, l)


def collect_combinations(candidates, blacklist, l):
    '''
    combines the head with the remaining elelements
    :param head:        first element to be concatenated with the left ones
    :param candidates:  rest of the elements to be concatenated with the head
    :param blacklist:
    :param l:
    :return:
    '''
    ret_list = []

    for x in itertools.combinations(candidates,l):

        if check_min(x, blacklist):
            ret_list.append(x)

    return ret_list


def check_min(candidate, blacklist):
    if len(blacklist) == 0:
        return True

    for black in blacklist:
        black_set = set(black)
        candidate_set = set(candidate)

        if black_set.issubset(candidate_set):
            return False

    return True


def memory_usage_own():
    import resource
    rusage_denom = 1024.
    if sys.platform == 'darwin':
        # ... it seems that in OSX the output is different units ...
        rusage_denom = rusage_denom * rusage_denom
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / rusage_denom
    return mem


def memory_usage_resource():
    import resource
    rusage_denom = 1024.
    if sys.platform == 'darwin':
        # ... it seems that in OSX the output is different units ...
        rusage_denom = rusage_denom * rusage_denom
    mem = resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss / rusage_denom
    return mem


if __name__ == "__main__":
    main()
